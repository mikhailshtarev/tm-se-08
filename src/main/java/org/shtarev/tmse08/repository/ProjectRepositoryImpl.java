package org.shtarev.tmse08.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements ProjectRepository<Project> {

    @NotNull
    private final HashMap<String, Project> projectMap = new HashMap<>();

    @Override
    public void persist(@NotNull final Project thisProject) {
        if (!projectMap.containsValue(thisProject))
            projectMap.put(thisProject.getId(), thisProject);
    }

    @Override
    public void remove(@NotNull final String Id, @NotNull final String userID) {
        @NotNull final Project thisProject = projectMap.get(Id);
        if (thisProject.getUserId().equals(userID)) {
            projectMap.remove(Id);
        }
    }

    @Override
    @Nullable
    public List<Project> findAll(@NotNull final String userID) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        return projectListValue.stream().filter(project -> project.getUserId().equals(userID)).collect(Collectors.toList());
    }

    @Override
    public void removeAll(@NotNull final String userID) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        @Nullable final List<Project> project2List = projectListValue.stream().filter(project -> project.getUserId()
                .equals(userID)).collect(Collectors.toList());
        for (Project thisProject : project2List) {
            projectMap.remove(thisProject.getId());
        }
    }
}

