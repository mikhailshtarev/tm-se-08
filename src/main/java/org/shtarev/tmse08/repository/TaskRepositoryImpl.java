package org.shtarev.tmse08.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements TaskRepository<Task> {
    @Nullable
    private final Map<String, Task> taskMap = new HashMap<>();

    @Override
    public void persist(@NotNull Task thisTask) {
        if (!taskMap.containsValue(thisTask))
            taskMap.put(thisTask.getId(), thisTask);
    }

    @Override
    public void remove(@NotNull final String id, @NotNull final String userId) {
        @Nullable final Task thisTask = taskMap.get(id);
        if (thisTask.getUserId().equals(userId)) {
            taskMap.remove(id);
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        return taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).forEach(x -> taskMap.remove(x.getId()));
    }
}


