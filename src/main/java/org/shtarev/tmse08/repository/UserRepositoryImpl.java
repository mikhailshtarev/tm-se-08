package org.shtarev.tmse08.repository;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepositoryImpl implements UserRepository<User> {

    @NotNull
    private final Map<String, User> userMap = new HashMap<>();

    @Override
    public void create(@NotNull final User thisUser) {
        userMap.put(thisUser.getUserId(), thisUser);
    }

    @Override
    @Nullable
    public List<User> getUserList() {
        return new ArrayList<>(userMap.values());
    }

    @Override
    public void rePassword(@NotNull final String userId, @NotNull final String newPassword) {
        @NotNull final User thisUser = userMap.get(userId);
        @NotNull final String newPassword1 = DigestUtils.md5Hex(newPassword);
        thisUser.setPassword(newPassword1);
    }

    @Override
    public void userUpdate(@NotNull String name, @NotNull String thisUserId) {
        @NotNull final User thisUser = userMap.get(thisUserId);
        thisUser.setName(name);
    }
}
