package org.shtarev.tmse08.repository;

import java.util.List;

public interface ProjectRepository<T> {

    void persist(final T thisProject);

    void remove(final String Id, final String userID);

    List<T> findAll(final String userID);

    void removeAll(final String userID);
}