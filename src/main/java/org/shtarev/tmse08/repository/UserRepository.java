package org.shtarev.tmse08.repository;

import org.shtarev.tmse08.entyty.User;

import java.util.List;

public interface UserRepository<U> {
    void create(final User thisUser);

    List<U> getUserList();

    void rePassword(final String userID, final String newPassword);

    void userUpdate(String name, String thisUserID);
}
