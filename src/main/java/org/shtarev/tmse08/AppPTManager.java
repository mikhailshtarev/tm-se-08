package org.shtarev.tmse08;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse08.Сommands.*;

public class AppPTManager {

    final static Class[] classes = {HelpCommand.class, ProjectListCommand.class, ProjectCreateCommand.class,
            ProjectClearCommands.class, ProjectRemoveCommand.class, TaskListCommand.class, TaskCreateCommand.class,
            TaskClearCommand.class, TaskRemoveCommand.class, UserList.class, UserCreate.class, UserLogIn.class,
            UserLogOut.class, UserRePassword.class, UserUpdate.class, AboutVersion.class};

    public static void main(String[] args) {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.createTwoUser();
        bootstrap.init();
        bootstrap.start();

    }
}
