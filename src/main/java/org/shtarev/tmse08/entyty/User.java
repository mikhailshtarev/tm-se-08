package org.shtarev.tmse08.entyty;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.Сommands.UserRole;

import java.util.UUID;

public class User {
    @Nullable
    @Getter
    @Setter
    private String name;

    @Nullable
    @Getter
    @Setter
    private String password;

    @Nullable
    @Getter
    @Setter
    private UserRole[] userRole;

    @Nullable
    @Getter
    private final String userId = UUID.randomUUID().toString();


}
