package org.shtarev.tmse08.entyty;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.util.UUID;

public class Task {

    @Nullable
    @Getter
    private final String id = UUID.randomUUID().toString();

    @Nullable
    @Getter
    @Setter
    private String name;
    @Nullable
    @Getter
    @Setter
    private String description;

    @Nullable
    @Getter
    @Setter
    private LocalDate dataStart;

    @Nullable
    @Getter
    @Setter
    private LocalDate dataFinish;

    @Nullable
    @Getter
    @Setter
    private String projectId;

    @Nullable
    @Getter
    @Setter
    private String UserId;


}