package org.shtarev.tmse08.Сommands;

import org.jetbrains.annotations.NotNull;

final public class TaskCreateCommand extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "TaskCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите название задачи:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание задачи:");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи в формате DD-MM-YYYY: ");
        @NotNull final String dataStart = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи в формате DD-MM-YYYY: ");
        @NotNull  final String dataFinish = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите ID проекта, к которому относится задача: ");
        @NotNull  final  String projectID = serviceLocator.getTerminalService().nextLine();
        @NotNull final  String thisUserID = serviceLocator.getBootUser().getUserId();
        serviceLocator.getTaskService().create(name, description, dataStart, dataFinish, projectID,thisUserID);

    }
}
