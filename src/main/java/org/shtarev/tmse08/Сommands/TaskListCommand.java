package org.shtarev.tmse08.Сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse08.entyty.Task;

import java.util.List;

final public class TaskListCommand extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "TaskList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all Tasks";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().taskReadAll(serviceLocator.getBootUser().getUserId());
        taskList.forEach(task -> System.out.println("Название задачи: " + task.getName() + "  ID задачи:  " + task.getId() +
                "  ID проекта к которому относится задача:  " + task.getProjectId()));

    }
}
