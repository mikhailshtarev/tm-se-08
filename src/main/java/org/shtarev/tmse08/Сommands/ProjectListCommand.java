package org.shtarev.tmse08.Сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse08.entyty.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute()  {
        System.out.println("[PROJECT LIST]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().projectsReadAll(serviceLocator.getBootUser()
                .getUserId());
        projectList.forEach(project -> System.out.println("Название проекта: " + project.getName() + "  ID проекта: " + project.getId()+
                "  ID User: " + project.getUserId()));
    }
}
