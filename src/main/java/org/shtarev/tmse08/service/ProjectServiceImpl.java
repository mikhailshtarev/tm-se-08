package org.shtarev.tmse08.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.Project;
import org.shtarev.tmse08.entyty.Task;
import org.shtarev.tmse08.repository.ProjectRepository;
import org.shtarev.tmse08.repository.TaskRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;


public class ProjectServiceImpl implements ProjectService<Project> {

    private final ProjectRepository<Project> projectRepository;

    private final TaskRepository<Task> taskRepository;
    @NotNull
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    @NotNull
    public ProjectServiceImpl(ProjectRepository<Project> projectRepository, TaskRepository<Task> taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String name, @Nullable final String description, @Nullable final String startDate
            , @Nullable final String finishDate, @Nullable final String thisUserId) {
        try {
            if (name.isEmpty() || description.isEmpty() || startDate.isEmpty() || finishDate.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            @NotNull final LocalDate startDateTR = LocalDate.parse(startDate, dateTimeFormatter);
            @NotNull final LocalDate finishDateTR = LocalDate.parse(finishDate, dateTimeFormatter);
            @NotNull final Project thisProject = new Project();
            thisProject.setName(name);
            thisProject.setDescription(description);
            thisProject.setDataStart(startDateTR);
            thisProject.setDataFinish(finishDateTR);
            thisProject.setUserId(thisUserId);
            projectRepository.persist(thisProject);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project do not create. Error");
        }
    }

    @Override
    public void delete(@Nullable final String id, @Nullable final String userID) {
        try {
            if (id.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            projectRepository.remove(id, userID);
            taskRepository.remove(id, userID);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project is not in the list. Error");
        }
    }

    @Override
    @Nullable
    public List<Project> projectsReadAll(@NotNull final String userID) {
        return projectRepository.findAll(userID);
    }

    @Override
    public void projectDeleteAll(@NotNull final String userId) {
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }
}

