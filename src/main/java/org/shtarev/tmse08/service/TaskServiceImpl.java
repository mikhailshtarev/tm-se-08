package org.shtarev.tmse08.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.Task;
import org.shtarev.tmse08.repository.TaskRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;

public class TaskServiceImpl implements TaskService<Task> {
    @NotNull
    private final TaskRepository<Task> taskRepository;
    @NotNull
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    @NotNull
    public TaskServiceImpl(final TaskRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String name, @Nullable final String description, @Nullable final String dataStart,
                       @Nullable final String dataFinish, @Nullable final String projectID,
                       @Nullable final String thisUserID)
    {
        try {
            if (name.isEmpty() || description.isEmpty() || dataStart.isEmpty() || dataFinish.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            @NotNull final LocalDate dataStartTR = LocalDate.parse(dataStart, dateTimeFormatter);
            @NotNull final LocalDate dataFinishTR = LocalDate.parse(dataFinish, dateTimeFormatter);
            @NotNull final Task thisTask = new Task();
            thisTask.setName(name);
            thisTask.setDescription(description);
            thisTask.setDataStart(dataStartTR);
            thisTask.setDataFinish(dataFinishTR);
            thisTask.setProjectId(projectID);
            thisTask.setUserId(thisUserID);
            taskRepository.persist(thisTask);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "! This task do not create. Error");
        }
    }

    @Override
    public void delete(@Nullable final String id,@Nullable final String userId) {
        try {
            if (id.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            taskRepository.remove(id, userId);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project is not in the list. Error");
        }
    }

    @Override
    @Nullable
    public List<Task> taskReadAll(@NotNull final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public void taskDeleteAll(@NotNull final String userId) {
        taskRepository.removeAll(userId);
    }
}





