package org.shtarev.tmse08.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse08.entyty.User;
import org.shtarev.tmse08.repository.UserRepository;
import org.shtarev.tmse08.Сommands.UserRole;

import java.util.List;

public class UserServiceImpl implements UserService<User> {

    @NotNull
    private final UserRepository<User> userRepository;


    @NotNull
    public UserServiceImpl(@NotNull UserRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    public void create(@Nullable final String name, @Nullable final String password, @Nullable final UserRole[] userRole) {
        try {
            if (name.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            @NotNull final User thisUser = new User();
            thisUser.setName(name);
            thisUser.setPassword(DigestUtils.md5Hex(password));
            thisUser.setUserRole(userRole);
            userRepository.create(thisUser);
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "Error");
        }
    }

    @Nullable
    public List<User> getUserList() {
        return userRepository.getUserList();
    }

    @Override
    @Nullable
    public User LogIn(@Nullable final String name, @Nullable final String password) {
        @NotNull final User newUser = new User();
        try {
            if (name.isEmpty() || password.isEmpty()) {
                System.out.println("One of the fields is empty, please try again");
                return newUser;
            }
            @NotNull final List<User> userListUL = userRepository.getUserList();
            @NotNull final String thisPassword = DigestUtils.md5Hex(password);
            for (User user : userListUL) {
                if (user.getName().equals(name) || user.getPassword().equals(thisPassword)) {
                    return user;
                }
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }
        System.out.println("Incorrect name or password, please try again.");
        return newUser;
    }

    @Override
    public void rePassword(@Nullable String name,@Nullable String oldPassword,@Nullable String newPassword) {
        try {
            if (name.isEmpty() || newPassword.isEmpty()) {
                System.out.println("One of the fields is empty, please try again");
                return;
            }
            @NotNull final List<User> userCollect = userRepository.getUserList();
            for (User thisUser : userCollect) {
                String oldPassword2 = DigestUtils.md5Hex(oldPassword);
                if (thisUser.getName().equals(name) || thisUser.getPassword().equals(oldPassword2)) {
                    userRepository.rePassword(thisUser.getUserId(), newPassword);
                } else System.out.println("Username or password does not match");
            }
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }

    }

    @Override
    public void userUpdate(@Nullable String name,@Nullable String thisUserId) {
        try {
            if (name.isEmpty() || thisUserId.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            userRepository.userUpdate(name, thisUserId);
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }
    }
}

